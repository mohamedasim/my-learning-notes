 <div class="form-group row">
                    <label for="Password" class="col-sm-4 col-form-label">ConfirmPassword</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="ConfirmPassword" formControlName="ConfirmPassword">
                      <div class="text-danger">
                        <small *ngIf="userLoginForm.get('ConfirmPassword')?.touched &&
                          userLoginForm.get('ConfirmPassword')?.hasError('required')" class="error-message">required.</small>
                          <span *ngIf="userLoginForm.hasError('notmatched')&&userLoginForm.get('ConfirmPassword')?.touched" class="error-block">
                  Password not match...</span>                      
                      </div>
                    </div>
                </div>


                  userLoginForm:FormGroup=this.fb.group({
    RecId:[null],
    EmployeeId:[null],
    Name:[null,[Validators.required,Validators.minLength(4)]],
    UserName:[null,[Validators.required,Validators.minLength(4)]],
    Password:[null,[Validators.required,Validators.minLength(4)]],
    ConfirmPassword:[null,[Validators.required,Validators.minLength(4)]],
    Email:[null,[Validators.email]],
    MobileNumber:[null,[Validators.minLength(10)]],
    Branch:[null],
    LodgeId:[null],
  },
  {validators: this.passwordMatchingValidator}
  );

  passwordMatchingValidator (group:FormGroup) {
    return group.get('Password')?.value === group.get('ConfirmPassword')?.value ? null :  { notmatched: true }
  }
